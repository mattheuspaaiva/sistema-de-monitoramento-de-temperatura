#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ArduinoOTA.h>
#include <ThingSpeak.h>
#include "CTBot.h"
#include "DHT.h"
#include <NTPClient.h>
#include <WiFiUDP.h>


#define DHTPIN D1 // pino que está coletando dados do sensor DHT11
#define DHTTYPE DHT11 // DHT 11

WiFiUDP udp;//Cria um objeto "UDP".
NTPClient ntp(udp, "a.st1.ntp.br", -3 * 3600, 60000);//Cria um objeto "NTP" com as configurações.

String hora;//Váriavel que armazenara o horario do NTP.

WiFiClient client;
CTBot myBot;
DHT dht(DHTPIN, DHTTYPE);

//Variáveis para controle de tempo para envio de alertas recorrentes
int horafun = 0;
int horaat = 0;

int flag = 0;
int flag2 = 3;
int flag3 = 0;
int flag4 = 0;
float t = 0; //variável para armazenar temperatura do sensor

//variáveis para armazenar alertas a serem enviadas no Telegram
String aviso;
TBMessage msg;

//variáveis para controlar o envio de dados para o ThingSpeak
unsigned long anterior = 0;
unsigned long intervalo = 300000;
unsigned long atual = 0;

//
int horas = 0;
int minutos = 0;

//
bool estado;
String postStr;
int timer = 0;

//Definicoes da rede wifi e conexao
String ssid  = "Nome da rede wifi";
String pass  = "Senha da sua rede wifi";
String token = "Token bot Telegram";


//Credenciais ThingSpeak
const char* host = "api.thingspeak.com";
String apiKey = "Chave canal ThingSpeak";


//função mandar msg em hora determinada
void mandar_msg(){
  if(horas == 9 && minutos == 00 && flag3 == 0)//Se a hora atual for igual à que definida, manda a temperatura.
   {
     myBot.sendMessage(msg.sender.id,"Temperatura: " +(String)(t)+  " ºC" );
     flag3 = 1;
   }
   if(horas == 17 && minutos == 00 && flag4 == 0){
     myBot.sendMessage(msg.sender.id,"Temperatura: " +(String)(t)+  " ºC" );
     flag4 = 1;
   }
   if(horas == 00){
      flag3 = 0;
      flag4 = 0;
   }
}

 
void mandar_alerta1(){
  if (t > 24.0 && t < 30.0 && flag == 0){
    horafun = ntp.getHours();
    aviso = (String)"[ALERTA] \nA temperatura é  " +(String)(t)+ " ºC. .";
    myBot.sendMessage(msg.sender.id, aviso);
    flag = 1;
    flag2 = 0;
  }
}
void mandar_alerta2(){
  if(t >= 30.0 && flag == 1){
    horafun = ntp.getHours();
    aviso = (String)"[ALERTA] \nA temperatura continua aumentando:  " +(String)(t)+ " ºC. ";
    myBot.sendMessage(msg.sender.id, aviso);
    flag = 2;
    flag2 = 0;
  }
}
void mandar_alerta3(){
  if((t > 35.0 && flag == 2) || t > 35 ){
    horafun = ntp.getHours();
    aviso = (String)"[ALERTA] \nNível crítico, os equipamentos podem sofrer avarias, a temperatura está a  " +(String)(t)+ " ºC.  ";
    myBot.sendMessage(msg.sender.id, aviso);
    flag = 3;
    flag2 = 0;
  }
}
void alerta_step(){
  aviso = (String)"[ALERTA] \nA temperatura é  " +(String)(t)+ " ºC. O ar-condicionado não está funcionando corretamente.";
  myBot.sendMessage(msg.sender.id, aviso);
  horafun = horaat;
}


//Envia os dados de leitura do sensor DHT 11 ao ThingSpeak
void envia_dados(){
  if (client.connect(host,80)){ // "184.106.153.149" or api.thingspeak.com
      postStr = apiKey;
      postStr +="&field1="; 
      postStr += String(t);
      postStr += "\r\n\r\n";
  
      client.print("POST /update HTTP/1.1\n");
      client.print("Host: api.thingspeak.com\n");
      client.print("Connection: close\n");
      client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n");
      client.print("Content-Type: application/x-www-form-urlencoded\n");
      client.print("Content-Length: ");
      client.print(postStr.length());
      client.print("\n\n");
      client.print(postStr);
  
      Serial.print("Temperature: ");
      Serial.print(t);
      Serial.print(" degrees Celcius");
      Serial.println(". Send to Thingspeak.");
   }
  client.stop();

  Serial.println("Waiting...");
  }
 // thingspeak precisa de no mínimo 15 segundos de espera entre envios


//Função: Restart NodeMCU
void restart_NodeMCU(int tempo){
    Serial.println("*O ESP ira resetar agora");
    timer = 0;
    delay(1000*10*tempo);
    ESP.reset();
}

void setup(){
  
  Serial.begin(115200);
  Serial.println("Inicializando bot Telegram...");
  dht.begin();
 
  //Conexao na rede wifi
  myBot.wifiConnect(ssid, pass);

  //Define o token
  myBot.setTelegramToken(token);

  //Verifica a conexao
  if (myBot.testConnection())
    Serial.println("\nConexao Ok!");
  else
    Serial.println("\nFalha na conexao!");

  ntp.begin();//Inicia o NTP.
  ntp.forceUpdate();//Força o Update.

  //Atualização de firmware via WiFi OTA
  
  // Porta padrao do ESP8266 para OTA eh 8266
  // ArduinoOTA.setPort(8266);
 
  // O Hostname padrao eh esp8266-[ChipID], mas voce pode mudar com essa funcao
  // ArduinoOTA.setHostname("nome_do_meu_esp8266");
 
  // Nenhuma senha eh pedida, mas voce pode dar mais seguranca pedindo uma senha pra gravar
  // ArduinoOTA.setPassword((const char *)"123");
  ArduinoOTA.setPassword((const char *)"senha de proteção");
 
  ArduinoOTA.onStart([]() {
    Serial.println("Inicio...");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("nFim!");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progresso: %u%%r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Erro [%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Autenticacao Falhou");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Falha no Inicio");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Falha na Conexao");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Falha na Recepcao");
    else if (error == OTA_END_ERROR) Serial.println("Falha no Fim");
  }); 
  
  ArduinoOTA.begin();
  Serial.println("Pronto");
  Serial.print("Endereco IP: ");
  Serial.println(WiFi.localIP()); 
}

void loop(){
  t = dht.readTemperature();
  hora = ntp.getFormattedTime();//Armazena na váriavel HORA, o horario atual.
  Serial.println(hora);
  ArduinoOTA.handle();
  atual = millis();
  horas = ntp.getHours();
  minutos = ntp.getMinutes(); 
  horaat = ntp.getHours();
     
  // testa se retorno é valido, caso contrário algo está errado.
  if (isnan(t)){
    Serial.println("Failed to read from DHT");
  } 
  else{
    Serial.print("Temperatura: ");
    Serial.print(t);
    Serial.println(" *C");
    Serial.print(flag);
    Serial.print("\n");
    
  }
  
  //Envia os dados de leitura do sensor DHT 11 ao ThingSpeak
  if(atual - anterior >= intervalo){
    envia_dados();
    anterior = atual;
  }
  
  //Detecta se novas mensagens foram enviadas do Bot telegram
  if (myBot.getNewMessage(msg)) {
    // there is a valid message in msg
    Serial.print("Received message from: ");
    Serial.println(msg.sender.username);
    if (msg.messageType == CTBotMessageText) {
      // a text message is received
      Serial.print("Text: ");
      Serial.println(msg.text);
    }
  }
  if (myBot.getNewMessage(msg)){
    if (msg.text.equalsIgnoreCase("iniciar")){
      myBot.sendMessage(msg.sender.id, "Conectado");
    }
  }
  if (myBot.getNewMessage(msg)){
    if (msg.text.equalsIgnoreCase("Temperatura")){
      aviso = (String)(t)+  " ºC";
      myBot.sendMessage(msg.sender.id, aviso);
    }
  }
  
  //Zera a flag caso a tempreratura do ambiente esteja a 20ºC
  if(t < 20.0){
    flag = 0;
  }
  
  //mensagem informativa
  mandar_msg();
  mandar_alerta1();
  mandar_alerta2();
  mandar_alerta3();
  
  //reconectarWiFI_Bot();

  // Manda um novo alerta duas horas após o promeiro alerta ter sido mandado
  if((horaat - horafun) == 2 && flag2 == 0){
    alerta_step();
    flag2 = 1;
  }
  
  Serial.print(horaat);
  Serial.print("\n");
  Serial.print(horafun);
  Serial.print("\n");
  
   
}
